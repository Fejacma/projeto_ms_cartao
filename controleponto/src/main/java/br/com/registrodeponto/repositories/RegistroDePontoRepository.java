package br.com.registrodeponto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.models.RegistroDePonto;

public interface RegistroDePontoRepository extends JpaRepository<RegistroDePonto, Long>{
	List<RegistroDePonto> findAllByResponsavel(Usuario dadosUsuario);
}
