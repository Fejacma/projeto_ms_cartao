package br.com.registrodeponto.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.registrodeponto.models.DTOs.UsuarioDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario create(UsuarioDTO usuarioDTO) {
		Usuario dadosUsuario = new Usuario();
		dadosUsuario.setNomeCompleto(usuarioDTO.getNomeCompleto());
		dadosUsuario.setCpf(usuarioDTO.getCpf());
		dadosUsuario.setEmail(usuarioDTO.getEmail());
		dadosUsuario.setDataDeCadastro(LocalDate.now());
		return usuarioRepository.save(dadosUsuario);

	}

	public List<UsuarioDTO> listarUsuarios() {
		List<UsuarioDTO> usuarioDTO = new ArrayList<UsuarioDTO>();

		usuarioRepository.findAll().forEach(usuario -> usuarioDTO.add(
				new UsuarioDTO(usuario.getNomeCompleto(), usuario.getCpf(), usuario.getEmail(), usuario.getDataDeCadastro())));
		;

		return usuarioDTO;
	}
	
	public UsuarioDTO getUsuario(long idUsuario) {
		Optional<Usuario> dadosPesquisa = usuarioRepository.findById(idUsuario);
		if(dadosPesquisa.isPresent()) {
			return new UsuarioDTO(dadosPesquisa.get().getNomeCompleto(), dadosPesquisa.get().getCpf(),
					dadosPesquisa.get().getEmail(), dadosPesquisa.get().getDataDeCadastro());
		}
		return null;
	}
	
	
	public Usuario update(long idUsuario, UsuarioDTO usuarioDTO) {
		Optional<Usuario> dadosPesquisa = usuarioRepository.findById(idUsuario);
		if(dadosPesquisa.isPresent()) {
			Usuario dadosUsuario = dadosPesquisa.get();
			dadosUsuario.setNomeCompleto(usuarioDTO.getNomeCompleto());
			dadosUsuario.setCpf(usuarioDTO.getCpf());
			dadosUsuario.setEmail(usuarioDTO.getEmail());
			return usuarioRepository.save(dadosUsuario);
		}else {
			return null;
		}
	}
}
