package br.com.registrodeponto.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.registrodeponto.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.registrodeponto.models.DTOs.UsuarioDTO;
import br.com.registrodeponto.models.Aluno;
import br.com.registrodeponto.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario create(UsuarioDTO alunoDTO) {
		Usuario dadosUsuario = new Usuario();
		dadosUsuario.setNomeCompleto(usuarioDTO.getNomeCompleto());
		dadosUsuario.setCpf(usuarioDTO.getCpf());
		dadosUsuario.setEmail(usuarioDTO.getEmail());
		dadosUsuario.setDataDeCadastro(LocalDate.now());
		return alunoRepository.save(dadosUsuario);

	}

	public List<UsuarioDTO> listarUsuarios() {
		List<UsuarioDTO> usuariosDTO = new ArrayList<UsuarioDTO>();

		usuarioRepository.findAll().forEach(usuario -> usuariosDTO.add(
				new UsuarioDTO(usuario.getNomeCompleto(), usuario.getCpf(), usuario.getEmail(), usuario.getDataDeCadastro())));
		;

		return usuariosDTO;
	}
	
	public UsuarioDTO getUsuario(long idUsuario) {
		Optional<Usuario> dadosPesquisa = usuarioRepository.findById(idUsuario);
		if(dadosPesquisa.isEmpty()) {
			return null;
		}
		return new UsuarioDTO(dadosPesquisa.get().getNomeCompleto(), dadosPesquisa.get().getCpf(), dadosPesquisa.get().getEmail(), dadosPesquisa.get().getDataDeCadastro());
	}
	
	
	public Usuario update(long idUsuario, UsuarioDTO usuarioDTO) {
		Optional<Usuario> dadosPesquisa = usuarioRepository.findById(idUsuario);
		if(dadosPesquisa.isEmpty()) {
			return null;
		}else {
			Usuario dadosUsuario = dadosPesquisa.get();
			dadosUsuario.setNomeCompleto(usuarioDTO.getNomeCompleto());
			dadosUsuario.setCpf(usuarioDTO.getCpf());
			dadosUsuario.setEmail(usuarioDTO.getEmail());
			return usuarioRepository.save(dadosUsuario);
		}
	}
}
