package br.com.registrodeponto.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.registrodeponto.models.DTOs.UsuarioDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.repositories.UsuarioRepository;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioServiceTest {
	@InjectMocks
	UsuarioService usuarioService;

	@Mock
	UsuarioRepository usuarioRepository;
	
	@Test
	public void testCreate() {
		Usuario dadosUsuario = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		UsuarioDTO dadosUsuarioDTO = new UsuarioDTO("Fernando",35207774875L,"teste@test.com",LocalDate.now());
		when(usuarioRepository.save(any(Usuario.class))).thenReturn(dadosUsuario);
		Usuario usuarioSalvo = usuarioService.create(dadosUsuarioDTO);
		assertTrue(dadosUsuario.getNomeCompleto().equals(usuarioSalvo.getNomeCompleto()));
	}
	@Test
	public void testGetUsuarios() {
		List<UsuarioDTO> dadosAlunoDTO = new ArrayList<UsuarioDTO>();
		dadosAlunoDTO.add(new UsuarioDTO("Fernando",35207774875L,"teste@test.com",LocalDate.now()));
		dadosAlunoDTO.add(new UsuarioDTO("Fernando Luchiari",35207774847L,"teste@teste.com",LocalDate.now()));
		
		List<Usuario> dadosAluno = new ArrayList<Usuario>();
		dadosAluno.add(new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now()));
		dadosAluno.add(new Usuario(2L,"Fernando Luchiari",35207774847L,"teste@teste.com",LocalDate.now()));
		
		when(usuarioRepository.findAll()).thenReturn(dadosAluno);
		
		List<UsuarioDTO> dadosAlunoDTOListados = usuarioService.listarUsuarios();
		
		assertTrue(dadosAlunoDTO.get(1).getCpf().equals(dadosAlunoDTOListados.get(1).getCpf()));		
	}
	
	@Test
	public void testGetUsuario() {
		Usuario dadosUsuario = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		UsuarioDTO dadosUsuarioDTO = new UsuarioDTO("Fernando",35207774875L,"teste@test.com",LocalDate.now());
		
		when(usuarioRepository.findById(1L)).thenReturn(Optional.of(dadosUsuario));
		UsuarioDTO alunoEncontrado = usuarioService.getUsuario(1L);
		
		assertTrue(dadosUsuarioDTO.getNomeCompleto().equals(alunoEncontrado.getNomeCompleto()));
		
	}
	
	@Test
	public void testUpdateUsuario() {
		Usuario dadosAluno = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		UsuarioDTO dadosAlunoDto = new UsuarioDTO("Fernando Luchiari",35207774875L,"teste@test.com",LocalDate.now());
		Usuario dadosAlunoAlterado = new Usuario(1L,"Fernando Luchiari",35207774875L,"teste@test.com",LocalDate.now());
		
		when(usuarioRepository.findById(1L)).thenReturn(Optional.of(dadosAluno));
		when(usuarioRepository.save(any(Usuario.class))).thenReturn(dadosAlunoAlterado);
		Usuario alunoEncontrado = usuarioService.update(1L,dadosAlunoDto);
		
		assertTrue(alunoEncontrado.getNomeCompleto().equals(dadosAlunoAlterado.getNomeCompleto()));
		
	}
	
}
