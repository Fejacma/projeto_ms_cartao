package br.com.controle.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import br.com.registrodeponto.controller.UsuarioController;
import br.com.registrodeponto.models.DTOs.UsuarioDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.response.Response;
import br.com.registrodeponto.service.UsuarioService;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioControllerTest {

	@InjectMocks
	UsuarioController usuarioController;

	@Mock
	UsuarioService usuarioService;

	@MockBean
	private BindingResult bindingResult;
	
	@Test
	public void createUsuarioTest() {
		Usuario dadosUsuario = new Usuario(1L,"Fernando",32036550800L,"teste@test.com",LocalDate.now());
		UsuarioDTO usuarioDTO = new UsuarioDTO("Fernando",32036550800L,"teste@test.com",LocalDate.now());
		
		bindingResult = Mockito.mock(BindingResult.class);
	    when(bindingResult.hasErrors()).thenReturn(false);
		when(usuarioService.create(usuarioDTO)).thenReturn(dadosUsuario);
		ResponseEntity<Response<String>> dadosRetorno = usuarioController.createUsuario(usuarioDTO, bindingResult);
		
		assertEquals(dadosRetorno.getStatusCode(),HttpStatus.OK);
	}
	
	@Test
	public void createUsuarioBadRequestTest() {
		UsuarioDTO usuarioDTO = new UsuarioDTO("Fernando",332036550800L,"teste@test.com",LocalDate.now());
		
		bindingResult = Mockito.mock(BindingResult.class);
	    when(bindingResult.hasErrors()).thenReturn(true);
		ResponseEntity<Response<String>> dadosRetorno = usuarioController.createUsuario(usuarioDTO, bindingResult);
		
		assertEquals(dadosRetorno.getStatusCode(),HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void getUsuarioTest() {
		
		UsuarioDTO usuarioDTO = new UsuarioDTO("Fernando",32036550800L,"teste@test.com",LocalDate.now());
		
		when(usuarioService.getUsuario(1L)).thenReturn(usuarioDTO);
		UsuarioDTO dadoRetorno = usuarioController.getUsuarios(1L);
		
		assertEquals(dadoRetorno.getCpf(),usuarioDTO.getCpf());
	}
	
	@Test
	public void getUsuariosTest() {
		List<UsuarioDTO> dadosUsuarioDTO = new ArrayList<UsuarioDTO>();
		dadosUsuarioDTO.add(new UsuarioDTO("Fernando",32036550800L,"teste@test.com",LocalDate.now()));
		dadosUsuarioDTO.add(new UsuarioDTO("Fernando Martini",32036550800L,"teste@teste.com",LocalDate.now()));
		
		when(usuarioService.listarUsuarios()).thenReturn(dadosUsuarioDTO);
		List<UsuarioDTO> dadosRetorno = usuarioController.getUsuarios();
		
		assertNotNull(dadosRetorno.get(1).getCpf());
	}
	
	@Test
	public void updateUsuarioTest() {
		UsuarioDTO usuarioDTO = new UsuarioDTO("Fernando",32036550800L,"teste@test.com",LocalDate.now());
		Usuario dadosUsuario = new Usuario(1L,"Fernando",32036550800L,"teste@test.com",LocalDate.now());
		
		
		when(usuarioService.update(1L,usuarioDTO)).thenReturn(dadosUsuario);
		ResponseEntity<Response<String>> dadosRetorno = usuarioController.updateUsuario(1L, usuarioDTO);
		
		assertEquals(dadosRetorno.getStatusCode(),HttpStatus.OK);
		
	}

}
