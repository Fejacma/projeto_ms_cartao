package br.com.itau.cliente.dtos;

import br.com.itau.cliente.models.Cliente;

public class CartaoDTOEntradaPost {

    private String numero;

    private Cliente cliente;

    public CartaoDTOEntradaPost() { }

    public String getNumero() { return numero; }

    public void setNumero(String numero) { this.numero = numero; }

    public Cliente getCliente() { return cliente; }

    public void setCliente(Cliente cliente) { this.cliente = cliente; }

}
