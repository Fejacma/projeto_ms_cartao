package controllers;

import models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import services.ClienteService;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.salvarCliente(cliente);
        return clienteObjeto;
    }

    @GetMapping("/{id}")
    public Cliente bucarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarPorId(id);
            return cliente;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
