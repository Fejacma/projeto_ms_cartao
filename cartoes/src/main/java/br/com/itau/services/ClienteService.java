package services;

import models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.ClienteRepository;

import java.util.Optional;

@Service
public class ClienteService{

    @Autowired
    private ClienteRepository clienteRepository;

//    @Autowired
//    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Cliente salvarCliente(Cliente cliente) {
//        String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());
//        usuario.setSenha(encoder);
        Cliente clienteObjeto = clienteRepository.save(cliente);

        return clienteObjeto;
    }

    public Cliente buscarPorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if (optionalCliente.isPresent()){

            return optionalCliente.get();
        }
        throw new RuntimeException("O Cliente não foi encontrado");
    }

}
