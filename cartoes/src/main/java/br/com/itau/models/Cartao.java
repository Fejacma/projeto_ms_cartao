package models;

import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int numero_Cartao;
    private String status_Cartao;

    @OneToOne
    private Cliente cliente_Cartao;

    public Cartao() {}

    public Cartao(int id, int numero_Cartao, String status_Cartao, Cliente cliente_Cartao) {
        this.id = id;
        this.numero_Cartao = numero_Cartao;
        this.status_Cartao = status_Cartao;
        this.cliente_Cartao = cliente_Cartao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero_Cartao() {
        return numero_Cartao;
    }

    public void setNumero_Cartao(int numero_Cartao) {
        this.numero_Cartao = numero_Cartao;
    }

    public String getStatus_Cartao() {
        return status_Cartao;
    }

    public void setStatus_Cartao(String status_Cartao) {
        this.status_Cartao = status_Cartao;
    }

    public Cliente getCliente_Cartao() {
        return cliente_Cartao;
    }

    public void setCliente_Cartao(Cliente cliente_Cartao) {
        this.cliente_Cartao = cliente_Cartao;
    }
}
